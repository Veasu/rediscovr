const webpack = require('webpack')
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;

module.exports = {
    chainWebpack: config => {
        config
        .plugin('html')
        .tap(args => {
          args[0].title = 'Redsicovr - Discovr Your Music'
          return args
        })
      },

    configureWebpack: {
        plugins: [
            new webpack.IgnorePlugin({
            resourceRegExp: /^\.\/locale$/,
            contextRegExp: /moment$/
            })
        ],
        optimization: {
            splitChunks: {
              minSize: 10000,
              maxSize: 250000,
            }
        }
    },

    devServer: {    
        proxy: {
            '/api': {
                target: 'http://localhost:3000',
                pathRewrite: {'^/api' : ''}
            },
            '/spotify/auth': {
                target: 'http://localhost:3000'
            },
        }
    },

    productionSourceMap: false
}