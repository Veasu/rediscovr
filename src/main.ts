import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {LayoutPlugin} from 'bootstrap-vue'

import './css/custom.scss'
import 'bootstrap-vue/dist/bootstrap-vue.min.css'

Vue.use(VueAxios, axios)
Vue.use(LayoutPlugin);
Vue.config.productionTip = false

store.subscribe((mutation, state) => {
    sessionStorage.setItem('store', JSON.stringify(state));
});

new Vue({
  router,
  store,
  beforeCreate() {
    this.$store.commit('initialiseStore');
  },
  render: h => h(App)
}).$mount('#app')
