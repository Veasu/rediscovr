/* eslint-disable @typescript-eslint/camelcase */

import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import { User, BackupSetting, PlaylistToBackup, PlaylistInfo} from '../types/helpers';

Vue.use(Vuex)

export interface StoreLayout {
    logged: boolean;
	user: User;
	redirectPath: string;
	currentPlaylistId: number;
	spotifyPlaylists: PlaylistToBackup[];
	onPlaylist: boolean;
}

function capitalizeFirstLetter(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const store: StoreOptions<StoreLayout> = {
	state: {
		logged: false,
		user: {
			userID:"",
			username: "",
			accessToken:"",
			refreshToken:"",
			expires_in: 0,
			playlists: [],
			backups: [],
			backupSetting: null,
			backupPlaylistId: ""
		},
		redirectPath: "",
		currentPlaylistId: -1,
		spotifyPlaylists: [],
		onPlaylist: false,
	},
	mutations: {
		pushAccessToken (state, accessToken: string) {
			state.user.accessToken = accessToken;
		},

		pushRefreshToken (state, refreshToken: string) {
			state.user.refreshToken = refreshToken;
		},

		pushLoginState (state, status: boolean) {
			state.logged = status;
		},

		pushTokenLife (state, tokenLife: number) {
			state.user.expires_in = tokenLife;
		},

		pushPlaylist (state, playlist: PlaylistInfo) {
			if (!state.user.backups)
			{
				state.user.backups = [];
			}

			if(!state.user.backups.some(el => el.playlistId == playlist.playlistId))
			{
				state.user.backups.push(playlist);
			}
		},

		pushPlaylists (state, playlist: PlaylistInfo[]) {
			state.user.backups = playlist;
		},

		pushCurrentPlaylistId(state, id: number)
		{
			state.currentPlaylistId = id;
		},

		pushUser(state, user: User)
		{
			state.user = user;
		},

		pushRedirectPath(state, path: string)
		{
			state.redirectPath = path;
		},

		pushPlaylistsToBackup(state, playlists: PlaylistToBackup[])
		{
			state.user.playlists = playlists;
		},

		pushBackupSetting(state, backupSetting: BackupSetting | null)
		{
			state.user.backupSetting = backupSetting;
		},

		pushBackupPlaylistId(state, backupPlaylistId: string)
		{
			state.user.backupPlaylistId = backupPlaylistId;
		},

		pushSpotifyPlaylists(state, spotifyPlaylists: PlaylistToBackup[])
		{
			state.spotifyPlaylists = spotifyPlaylists;
		},

		pushOnPlaylist(state, onPlaylist: boolean)
		{
			state.onPlaylist = onPlaylist;
		},

		initialiseStore(state) {
			if(sessionStorage.getItem('store')) {
				this.replaceState(
					Object.assign(state, JSON.parse(sessionStorage.getItem('store') as string))
				);
			}
		}
	},
	actions: {
	},
	modules: {
	},
	getters: {
		logged: state => state.logged,
		accessToken: state => state.user.accessToken,
		refreshToken: state => state.user.refreshToken,
		tokenLife: state =>  state.user.expires_in - Math.floor(Date.now()/1000),
		playlists: state => state.user.playlists,
		backups: state => state.user.backups,
		currentPlaylist: state => state.user.backups![state.currentPlaylistId],
		currentPlaylistTracks: state => state.user.backups![state.currentPlaylistId].playlistTracks,
		username: state => state.user.username,
		usernameUpper: state => state.user.username ? capitalizeFirstLetter(state.user.username) : "",
		redirectPath: state => state.redirectPath == '' ?  '/profile/playlists' : state.redirectPath,
		backupSetting: state => state.user.backupSetting,
		backupPlaylistId: state => state.user.backupPlaylistId,
		onPlaylist: state => state.onPlaylist
	}
}

export default new Vuex.Store<StoreLayout>(store)
