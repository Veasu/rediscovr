import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import FrontPage from '../views/FrontPage.vue'
const Callback = () => import('../views/Callback.vue')
const Profile = () => import('../views/Profile.vue')
const Playlist = () => import('../views/Playlist.vue')
const Playlists = () => import('../views/Playlists.vue')
const ProfileSettings = () => import('../views/ProfileSettings.vue')
const Logs = () => import('../views/Logs.vue')


Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
    {
      path: '/',
      name: 'FrontPage',
      component: FrontPage
    },
    {
      path: '/home',
      name: 'FrontPage',
      component: FrontPage
    },
    {
      path: '/callback',
      name: 'Callback',
      component: Callback
    },
    {
      path: '/profile',
      name: 'Profile',
      children: [{
          path: 'playlists',
          name: "Playlists",
          component: Playlists
        }, {
          path: 'settings',
          name: 'Settings',
          component: ProfileSettings
        },{
          path: 'playlist',
          name: 'Playlist',
          component: Playlist
        },{
          path: 'logs',
          name: 'Logs',
          component: Logs
        },
      ],
      component: Profile
    },
    {
      path: '*',
      redirect: '/'
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
