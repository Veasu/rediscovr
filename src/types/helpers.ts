export enum BackupSetting {
    LOCAL,
    NEW_PLAYLIST,
    APPEND_PLAYLIST,
    ADD_TO_LIBRARY,
    NO_BACKUP
}

export enum BackupType {
    FORCED,
    AUTOMATIC
}

export interface PlaylistToBackup {
    playlistId: string;
    playlistName: string;
    playlistDesc: string;
    playlistURI: string;
}

export interface PlaylistInfo extends PlaylistToBackup{
    playlistDate?: number;
    playlistImage: string;
    playlistTracks: PlaylistTrackItem[];
}

export interface PlaylistTrackItem{
    artist: ArtistItem[];
    track: TrackItem; 
    album: AlbumItem; 
    duration: string;
}

export interface ArtistItem{
    artistName: string;
    artistURI: string;
}

export interface TrackItem{
    trackName: string;
    explicit: boolean;
    trackURI: string;
}

export interface AlbumItem{
    albumName: string;
    albumURI: string;
}

export interface User {
    userID: string;
    username: string;
    accessToken: string;
    refreshToken: string;
    expires_in: number;
    playlists: PlaylistToBackup[];
    backups?: PlaylistInfo[];
    backupSetting: BackupSetting | null;
    backupPlaylistId: string;
}

export interface BackupLogs {
    userID: string;
    backupTime: number;
    success: boolean;
    backupSetting: BackupSetting;
    backupMessage?: string;
    backupType: BackupType;
}